import { currentActionsNames } from './globals';
import { useReactiveChannelAction as subscribeReactiveChannelAction, makeChannelAction } from './lib'
import { ReactiveValue } from './lib/types';
import { PubSubActionInterceptor } from './types';
import { deepFreeze } from './utils';

/**
 * Generic parameter Config is required!
 * ```tsx
 * const fixedPubSub = createFixedPubSub<PubSubConfig>('pubSubName')
 * ```
 */
export const createFixedPubSub = <Config extends { channel: string, dataType: unknown } = never>(
    name: string & (Config extends never ? "You must provide a type parameter" : string),
    interceptors: PubSubActionInterceptor[] = []
) => {
    if (currentActionsNames.includes(name) && typeof window !== 'undefined') {
        console.warn(`PubSub Action with name ${name} already defined`)
    }

    type ConfigKey = Config["channel"];

    currentActionsNames.push(name)

    const action = makeChannelAction();

    return {
        publish: <K extends ConfigKey>(channel: K, value: Extract<Config, { channel: K }>["dataType"]) => {
            const dispatchValue = deepFreeze<ReactiveValue<Extract<Config, { channel: K }>["dataType"]>>({ payload: value })

            action.dispatchToChannel(channel, dispatchValue)

            interceptors.forEach(interceptor => interceptor.onDispatch?.(name, channel, dispatchValue.payload as Readonly<Extract<Config, { channel: K }>["dataType"]>))
        },
        subscribe: <K extends ConfigKey>(channel: K, callback: (value: Readonly<Extract<Config, { channel: K }>["dataType"]>) => void) => subscribeReactiveChannelAction(action, channel, callback)
    }
}
