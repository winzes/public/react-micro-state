import { useEffect, useState } from "react";
import { StateValue } from "../../types";
import { ReactiveVar } from "../ReactiveVar";

export const useReactiveVar = <T>(reactiveVar: ReactiveVar<T>): Readonly<T> => {
    const [state, setState] = useState<Readonly<T>>(reactiveVar.getValue());

    useEffect(() => {
        const listener = (_oldState: Readonly<T | null>, newState: Readonly<T>) => {
            setState(newState)
        }

        reactiveVar.addListener(listener)

        return () => {
            reactiveVar.removeListener(listener)
        }
    }, [reactiveVar]);

    return state
}

export const useReactiveVarWithPrev = <T>(reactiveVar: ReactiveVar<T>): Readonly<StateValue<T>> => {
    const [state, setState] = useState<Readonly<StateValue<T>>>({
        current: reactiveVar.getValue() as Readonly<T>,
        prev: reactiveVar.getPrevValue() as Readonly<T> | null
    });

    useEffect(() => {
        const listener = (oldState: Readonly<T | null>, newState: Readonly<T>) => {
            setState({ prev: oldState, current: newState })
        }

        reactiveVar.addListener(listener)

        return () => {
            reactiveVar.removeListener(listener)
        }
    }, [reactiveVar]);

    return state
}
