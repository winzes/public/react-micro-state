import { useEffect } from "react";
import { ReactiveChannelAction } from "../ReactiveChannelAction";
import { ListenerFunction } from "../types";

export const useReactiveChannelAction = <T>(action: ReactiveChannelAction<T>, channel: string | number, callback: ListenerFunction<T>): void => {
    useEffect(() => {
        action.addListenerToChannel(channel, callback)

        return () => {
            action.removeListenerFromChannel(channel, callback)
        }
    }, [action, channel, callback]);
}
