import { useEffect } from "react";
import { ReactiveAction } from "../ReactiveAction";
import { ListenerFunction } from "../types";

export const useReactiveAction = <T>(action: ReactiveAction<T>, callback: ListenerFunction<T>): void => {
    useEffect(() => {
        action.addListener(callback)

        return () => {
            action.removeListener(callback)
        }
    }, [action, callback]);
}
