import { ListenerFunctionWithPrev, ReactiveValue } from "./types";

export class ReactiveVar<T> {
    protected Value: Readonly<ReactiveValue<T>>

    protected PrevValue: Readonly<ReactiveValue<T> | null>

    protected ListenerFunctions: Set<ListenerFunctionWithPrev> = new Set();

    constructor(initialState: ReactiveValue<T>) {
        this.Value = initialState

        this.PrevValue = null
    }

    public addListener(listener: ListenerFunctionWithPrev<T>): void {
        this.ListenerFunctions.add(listener)
    }

    public removeListener(listener: ListenerFunctionWithPrev<T>): void {
        this.ListenerFunctions.delete(listener)
    }

    public setValue(value: ReactiveValue<T>): void {
        const prevValue = this.Value.payload as Readonly<T>

        this.PrevValue = this.Value;

        this.Value = value

        this.ListenerFunctions.forEach(listener => listener(prevValue, this.Value.payload as Readonly<T>))
    }

    public getValue(): Readonly<T> {
        return this.Value.payload
    }

    public getPrevValue(): Readonly<T> | null {
        return !this.PrevValue ? null : this.PrevValue.payload
    }

    public reset(initialState: ReactiveValue<T>): void {
        this.Value = initialState

        this.PrevValue = null

        this.ListenerFunctions.forEach(listener => listener(this.PrevValue, this.Value.payload as Readonly<T>))
    }
}

export const makeVar = <T>(initialState: ReactiveValue<T>): ReactiveVar<T> => {
    return new ReactiveVar<T>(initialState)
}
