import { ListenerFunction, ReactiveValue } from "./types";

export class ReactiveChannelAction<T> {
    protected ChannelListenersMap: Map<string | number, Set<ListenerFunction>> = new Map<string | number, Set<ListenerFunction>>();

    public addListenerToChannel(channel: string | number, listener: ListenerFunction<T>): void {
        const listeners = this.ChannelListenersMap.get(channel)

        if (!listeners) {
            this.ChannelListenersMap.set(channel, new Set([listener]))

            return;
        }

        listeners.add(listener)
    }

    public removeListenerFromChannel(channel: string | number, listener: ListenerFunction<T>): void {
        const listeners = this.ChannelListenersMap.get(channel)

        if (!listeners) {
            return;
        }

        listeners.delete(listener)

        if (listeners.size === 0) {
            this.ChannelListenersMap.delete(channel)
        }
    }

    public dispatchToChannel(channel: string | number, value: ReactiveValue<T>): void {
        const dispatchValue = value?.payload as Readonly<T>;

        this.ChannelListenersMap.get(channel)?.forEach(listener => listener(dispatchValue))
    }
}

export const makeChannelAction = <T = any>(): ReactiveChannelAction<T> => {
    return new ReactiveChannelAction<T>()
}
