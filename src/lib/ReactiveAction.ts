import { ListenerFunction, ReactiveValue } from "./types";

export class ReactiveAction<T> {
    protected ListenerFunctions: Set<ListenerFunction> = new Set();

    public addListener(listener: ListenerFunction<T>): void {
        this.ListenerFunctions.add(listener)
    }

    public removeListener(listener: ListenerFunction<T>): void {
        this.ListenerFunctions.delete(listener)
    }

    public dispatch(value: ReactiveValue<T>): void {
        const dispatchValue = value?.payload as Readonly<T>

        this.ListenerFunctions.forEach(listener => listener(dispatchValue))
    }
}

export const makeAction = <T>(): ReactiveAction<T> => {
    return new ReactiveAction<T>()
}
