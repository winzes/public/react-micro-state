export type ReactiveValue<T> = { payload: T }

export type ListenerFunction<T = any> = (value: Readonly<T>) => void

export type ListenerFunctionWithPrev<T = any> = (prevState: Readonly<T | null>, newState: Readonly<T>) => void
