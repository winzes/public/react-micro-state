import { currentStatesNames } from './globals';
import { makeVar, useReactiveVar as getReactiveVar, useReactiveVarWithPrev as getReactiveVarWithPrev } from './lib'
import { ReactiveValue } from './lib/types';
import { StateInterceptor, StateValue } from './types';
import { deepFreeze } from './utils';

export type State<T> = Readonly<{
    set: (updateState: ((currentState: T) => T) | T) => void;
    subscribe: {
        (withPrev?: false): Readonly<T>;
        (withPrev: true): Readonly<StateValue<T>>;
    },
    get: {
        (withPrev?: false): Readonly<T>;
        (withPrev: true): Readonly<StateValue<T>>;
    },
    reset: () => void;
}>

export const createState = <T>(name: string, initialState: T, interceptors: StateInterceptor<T>[] = []): State<T> => {
    if (currentStatesNames.includes(name) && typeof window !== 'undefined') {
        console.warn(`Reactive State with name ${name} already defined`)
    }

    currentStatesNames.push(name)

    const state = makeVar<T>(deepFreeze<ReactiveValue<T>>({ payload: initialState }))

    const set = (updateState: ((currentState: T) => T) | T, initial?: boolean): void => {
        const prevValue = state.getValue();

        if (typeof updateState === 'function') {
            const newValue = deepFreeze<ReactiveValue<T>>({ payload: (updateState as (currentState: T) => T)(prevValue as Readonly<T>) });

            if (newValue.payload === prevValue) {
                return;
            }

            initial ? state.reset(newValue) : state.setValue(newValue)
        } else {
            const newValue = deepFreeze<ReactiveValue<T>>({ payload: updateState })

            if (newValue.payload === prevValue) {
                return;
            }

            initial ? state.reset(newValue) : state.setValue(newValue)
        }

        interceptors.forEach(interceptor => interceptor.onSet?.(name, state.getValue(), prevValue))
    }

    const reset = (): void => {
        state.reset(deepFreeze({ payload: initialState }))

        interceptors.forEach(interceptor => interceptor.onReset?.(name, state.getValue(), (updateState) => set(updateState, true)))
    }

    function get(withPrev?: false): Readonly<T>
    function get(withPrev: true): Readonly<StateValue<T>>
    function get(withPrev = false): Readonly<T> | Readonly<StateValue<T>> {
        return !withPrev ? state.getValue() : { prev: state.getPrevValue(), current: state.getValue() }
    }

    function subscribe(withPrev?: false): Readonly<T>
    function subscribe(withPrev: true): Readonly<StateValue<T>>
    function subscribe(withPrev = false): Readonly<T> | Readonly<StateValue<T>> {
        return !withPrev ? getReactiveVar(state) : getReactiveVarWithPrev(state)
    }

    interceptors.forEach(interceptor => interceptor.onInit?.(name, state.getValue(), (updateState) => set(updateState, true)))

    return {
        get,
        set: (updateState: ((currentState: T) => T) | T) => set(updateState),
        subscribe,
        reset
    }
}
