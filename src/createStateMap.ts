import { createState } from "./createState";
import { StateInterceptor } from "./types";

type StateMap<T> = Readonly<{
    set: (key: string | number, updateState: ((currentState: T) => T) | T) => void;
    subscribe: (key: string | number) => Readonly<T>;
    subscribeMap: () => Readonly<Omit<Map<string | number, Readonly<T>>, 'set' | 'delete' | 'clear'>>;
    get: (key: string | number) => Readonly<T>;
    getMap: () => Readonly<Omit<Map<string | number, Readonly<T>>, 'set' | 'delete' | 'clear'>>;
    reset: (key: string | number) => void;
    resetMap: () => void;
}>;

export const createStateMap = <T>(name: string, defaultValue: T, interceptors: StateInterceptor<Map<string | number, T>>[] = []): StateMap<T> => {
    const state = createState<Map<string | number, T>>(name, new Map<string | number, T>(), interceptors);

    const set = (key: string | number, updateState: ((currentState: T) => T) | T) => {
        const currentState = state.get();

        const currentValue = currentState.get(key) as Readonly<T> || defaultValue;

        if (typeof updateState === 'function') {
            const newValue = (updateState as (currentState: T) => T)(currentValue as Readonly<T>);

            if (newValue === currentValue) {
                return;
            }

            currentState.set(key, newValue);
        } else {
            const newValue = updateState;

            if (newValue === currentValue) {
                return;
            }

            currentState.set(key, newValue);
        }

        state.set(new Map(currentState));
    };

    const get = (key: string | number) => (state.get().get(key) || defaultValue) as Readonly<T>;

    const reset = (key: string | number) => {
        state.get().delete(key);

        state.set(new Map(state.get()));
    };

    const resetMap = () => state.set(new Map());

    const subscribe = (key: string | number) => (state.subscribe().get(key) || defaultValue) as Readonly<T>;

    const subscribeMap = () => state.subscribe()

    const getMap = () => state.get()
    
    return {
        set,
        subscribe,
        subscribeMap,
        get,
        reset,
        resetMap,
        getMap
    };
};
