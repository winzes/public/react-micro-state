import { currentActionsNames } from './globals';
import { useReactiveChannelAction as subscribeReactiveChannelAction, makeChannelAction } from './lib'
import { ReactiveValue } from './lib/types';
import { PubSubActionInterceptor } from './types';
import { deepFreeze } from './utils';

type Action = Readonly<{
    publish: <T>(channel: string | number, value: T) => void;
    subscribe: <T>(channel: string | number, callback: (value: Readonly<T>) => void) => void;
}>

export const createCustomPubSub = (name: string, interceptors: PubSubActionInterceptor[] = []): Action => {
    if (currentActionsNames.includes(name) && typeof window !== 'undefined') {
        console.warn(`PubSub Action with name ${name} already defined`)
    }

    currentActionsNames.push(name)

    const action = makeChannelAction();

    return {
        publish: <T>(channel: string | number, value: T) => {
            const dispatchValue = deepFreeze<ReactiveValue<T>>({ payload: value })
            
            action.dispatchToChannel(channel, dispatchValue)

            interceptors.forEach(interceptor => interceptor.onDispatch?.(name, channel, dispatchValue.payload as Readonly<T>))
        },
        subscribe: <T>(channel: string | number, callback: (value: Readonly<T>) => void) => subscribeReactiveChannelAction(action, channel, callback)
    }
}
