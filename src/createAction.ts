import { currentActionsNames } from './globals';
import { makeAction, useReactiveAction as subscribeReactiveAction } from './lib'
import { ReactiveValue } from './lib/types';
import { ActionInterceptor } from './types';
import { deepFreeze } from './utils';

type Action<T> = Readonly<{
    dispatch: (value: T) => void;
    subscribe: (callback: (value: Readonly<T>) => void) => void;
}>

export const createAction = <T>(name: string, interceptors: ActionInterceptor<T>[] = []): Action<T> => {
    if (currentActionsNames.includes(name) && typeof window !== 'undefined') {
        console.warn(`Reactive Action with name ${name} already defined`)
    }

    currentActionsNames.push(name)

    const action = makeAction<T>();

    return {
        dispatch: (value: T) => {
            const dispatchValue = deepFreeze<ReactiveValue<T>>({ payload: value })

            action.dispatch(dispatchValue)

            interceptors.forEach(interceptor => interceptor.onDispatch?.(name, dispatchValue.payload as Readonly<T>))            
        },
        subscribe: (callback: (value: Readonly<T>) => void) => subscribeReactiveAction(action, callback)
    }
}
