"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStateMap = void 0;
var createState_1 = require("./createState");
var createStateMap = function (name, defaultValue, interceptors) {
    if (interceptors === void 0) { interceptors = []; }
    var state = (0, createState_1.createState)(name, new Map(), interceptors);
    var set = function (key, updateState) {
        var currentState = state.get();
        var currentValue = currentState.get(key) || defaultValue;
        if (typeof updateState === 'function') {
            var newValue = updateState(currentValue);
            if (newValue === currentValue) {
                return;
            }
            currentState.set(key, newValue);
        }
        else {
            var newValue = updateState;
            if (newValue === currentValue) {
                return;
            }
            currentState.set(key, newValue);
        }
        state.set(new Map(currentState));
    };
    var get = function (key) { return (state.get().get(key) || defaultValue); };
    var reset = function (key) {
        state.get().delete(key);
        state.set(new Map(state.get()));
    };
    var resetMap = function () { return state.set(new Map()); };
    var subscribe = function (key) { return (state.subscribe().get(key) || defaultValue); };
    var subscribeMap = function () { return state.subscribe(); };
    var getMap = function () { return state.get(); };
    return {
        set: set,
        subscribe: subscribe,
        subscribeMap: subscribeMap,
        get: get,
        reset: reset,
        resetMap: resetMap,
        getMap: getMap
    };
};
exports.createStateMap = createStateMap;
//# sourceMappingURL=createStateMap.js.map