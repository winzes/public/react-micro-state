import { ActionInterceptor } from './types';
declare type Action<T> = Readonly<{
    dispatch: (value: T) => void;
    subscribe: (callback: (value: Readonly<T>) => void) => void;
}>;
export declare const createAction: <T>(name: string, interceptors?: Readonly<{
    onDispatch?: (name: string, value: T) => void;
}>[]) => Readonly<{
    dispatch: (value: T) => void;
    subscribe: (callback: (value: Readonly<T>) => void) => void;
}>;
export {};
