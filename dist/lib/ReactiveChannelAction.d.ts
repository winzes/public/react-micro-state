import { ListenerFunction, ReactiveValue } from "./types";
export declare class ReactiveChannelAction<T> {
    protected ChannelListenersMap: Map<string | number, Set<ListenerFunction>>;
    addListenerToChannel(channel: string | number, listener: ListenerFunction<T>): void;
    removeListenerFromChannel(channel: string | number, listener: ListenerFunction<T>): void;
    dispatchToChannel(channel: string | number, value: ReactiveValue<T>): void;
}
export declare const makeChannelAction: <T = any>() => ReactiveChannelAction<T>;
