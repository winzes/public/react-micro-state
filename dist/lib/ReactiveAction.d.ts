import { ListenerFunction, ReactiveValue } from "./types";
export declare class ReactiveAction<T> {
    protected ListenerFunctions: Set<ListenerFunction>;
    addListener(listener: ListenerFunction<T>): void;
    removeListener(listener: ListenerFunction<T>): void;
    dispatch(value: ReactiveValue<T>): void;
}
export declare const makeAction: <T>() => ReactiveAction<T>;
