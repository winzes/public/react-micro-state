"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeVar = exports.ReactiveVar = void 0;
var ReactiveVar = /** @class */ (function () {
    function ReactiveVar(initialState) {
        this.ListenerFunctions = new Set();
        this.Value = initialState;
        this.PrevValue = null;
    }
    ReactiveVar.prototype.addListener = function (listener) {
        this.ListenerFunctions.add(listener);
    };
    ReactiveVar.prototype.removeListener = function (listener) {
        this.ListenerFunctions.delete(listener);
    };
    ReactiveVar.prototype.setValue = function (value) {
        var _this = this;
        var prevValue = this.Value.payload;
        this.PrevValue = this.Value;
        this.Value = value;
        this.ListenerFunctions.forEach(function (listener) { return listener(prevValue, _this.Value.payload); });
    };
    ReactiveVar.prototype.getValue = function () {
        return this.Value.payload;
    };
    ReactiveVar.prototype.getPrevValue = function () {
        return !this.PrevValue ? null : this.PrevValue.payload;
    };
    ReactiveVar.prototype.reset = function (initialState) {
        var _this = this;
        this.Value = initialState;
        this.PrevValue = null;
        this.ListenerFunctions.forEach(function (listener) { return listener(_this.PrevValue, _this.Value.payload); });
    };
    return ReactiveVar;
}());
exports.ReactiveVar = ReactiveVar;
var makeVar = function (initialState) {
    return new ReactiveVar(initialState);
};
exports.makeVar = makeVar;
//# sourceMappingURL=ReactiveVar.js.map