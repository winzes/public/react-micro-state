export declare type ReactiveValue<T> = {
    payload: T;
};
export declare type ListenerFunction<T = any> = (value: Readonly<T>) => void;
export declare type ListenerFunctionWithPrev<T = any> = (prevState: Readonly<T | null>, newState: Readonly<T>) => void;
