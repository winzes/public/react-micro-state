import { ListenerFunctionWithPrev, ReactiveValue } from "./types";
export declare class ReactiveVar<T> {
    protected Value: Readonly<ReactiveValue<T>>;
    protected PrevValue: Readonly<ReactiveValue<T> | null>;
    protected ListenerFunctions: Set<ListenerFunctionWithPrev>;
    constructor(initialState: ReactiveValue<T>);
    addListener(listener: ListenerFunctionWithPrev<T>): void;
    removeListener(listener: ListenerFunctionWithPrev<T>): void;
    setValue(value: ReactiveValue<T>): void;
    getValue(): Readonly<T>;
    getPrevValue(): Readonly<T> | null;
    reset(initialState: ReactiveValue<T>): void;
}
export declare const makeVar: <T>(initialState: ReactiveValue<T>) => ReactiveVar<T>;
