import { ReactiveAction } from "../ReactiveAction";
import { ListenerFunction } from "../types";
export declare const useReactiveAction: <T>(action: ReactiveAction<T>, callback: ListenerFunction<T>) => void;
