"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useReactiveAction = void 0;
var react_1 = require("react");
var useReactiveAction = function (action, callback) {
    (0, react_1.useEffect)(function () {
        action.addListener(callback);
        return function () {
            action.removeListener(callback);
        };
    }, [action, callback]);
};
exports.useReactiveAction = useReactiveAction;
//# sourceMappingURL=useReactiveAction.js.map