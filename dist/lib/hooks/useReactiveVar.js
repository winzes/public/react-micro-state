"use strict";
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useReactiveVarWithPrev = exports.useReactiveVar = void 0;
var react_1 = require("react");
var useReactiveVar = function (reactiveVar) {
    var _a = __read((0, react_1.useState)(reactiveVar.getValue()), 2), state = _a[0], setState = _a[1];
    (0, react_1.useEffect)(function () {
        var listener = function (_oldState, newState) {
            setState(newState);
        };
        reactiveVar.addListener(listener);
        return function () {
            reactiveVar.removeListener(listener);
        };
    }, [reactiveVar]);
    return state;
};
exports.useReactiveVar = useReactiveVar;
var useReactiveVarWithPrev = function (reactiveVar) {
    var _a = __read((0, react_1.useState)({
        current: reactiveVar.getValue(),
        prev: reactiveVar.getPrevValue()
    }), 2), state = _a[0], setState = _a[1];
    (0, react_1.useEffect)(function () {
        var listener = function (oldState, newState) {
            setState({ prev: oldState, current: newState });
        };
        reactiveVar.addListener(listener);
        return function () {
            reactiveVar.removeListener(listener);
        };
    }, [reactiveVar]);
    return state;
};
exports.useReactiveVarWithPrev = useReactiveVarWithPrev;
//# sourceMappingURL=useReactiveVar.js.map