"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useReactiveChannelAction = void 0;
var react_1 = require("react");
var useReactiveChannelAction = function (action, channel, callback) {
    (0, react_1.useEffect)(function () {
        action.addListenerToChannel(channel, callback);
        return function () {
            action.removeListenerFromChannel(channel, callback);
        };
    }, [action, channel, callback]);
};
exports.useReactiveChannelAction = useReactiveChannelAction;
//# sourceMappingURL=useReactiveChannelAction.js.map