import { ReactiveChannelAction } from "../ReactiveChannelAction";
import { ListenerFunction } from "../types";
export declare const useReactiveChannelAction: <T>(action: ReactiveChannelAction<T>, channel: string | number, callback: ListenerFunction<T>) => void;
