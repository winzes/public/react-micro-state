import { StateValue } from "../../types";
import { ReactiveVar } from "../ReactiveVar";
export declare const useReactiveVar: <T>(reactiveVar: ReactiveVar<T>) => Readonly<T>;
export declare const useReactiveVarWithPrev: <T>(reactiveVar: ReactiveVar<T>) => Readonly<StateValue<T>>;
