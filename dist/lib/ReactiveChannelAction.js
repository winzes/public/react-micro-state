"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeChannelAction = exports.ReactiveChannelAction = void 0;
var ReactiveChannelAction = /** @class */ (function () {
    function ReactiveChannelAction() {
        this.ChannelListenersMap = new Map();
    }
    ReactiveChannelAction.prototype.addListenerToChannel = function (channel, listener) {
        var listeners = this.ChannelListenersMap.get(channel);
        if (!listeners) {
            this.ChannelListenersMap.set(channel, new Set([listener]));
            return;
        }
        listeners.add(listener);
    };
    ReactiveChannelAction.prototype.removeListenerFromChannel = function (channel, listener) {
        var listeners = this.ChannelListenersMap.get(channel);
        if (!listeners) {
            return;
        }
        listeners.delete(listener);
        if (listeners.size === 0) {
            this.ChannelListenersMap.delete(channel);
        }
    };
    ReactiveChannelAction.prototype.dispatchToChannel = function (channel, value) {
        var _a;
        var dispatchValue = value === null || value === void 0 ? void 0 : value.payload;
        (_a = this.ChannelListenersMap.get(channel)) === null || _a === void 0 ? void 0 : _a.forEach(function (listener) { return listener(dispatchValue); });
    };
    return ReactiveChannelAction;
}());
exports.ReactiveChannelAction = ReactiveChannelAction;
var makeChannelAction = function () {
    return new ReactiveChannelAction();
};
exports.makeChannelAction = makeChannelAction;
//# sourceMappingURL=ReactiveChannelAction.js.map