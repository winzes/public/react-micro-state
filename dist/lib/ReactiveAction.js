"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeAction = exports.ReactiveAction = void 0;
var ReactiveAction = /** @class */ (function () {
    function ReactiveAction() {
        this.ListenerFunctions = new Set();
    }
    ReactiveAction.prototype.addListener = function (listener) {
        this.ListenerFunctions.add(listener);
    };
    ReactiveAction.prototype.removeListener = function (listener) {
        this.ListenerFunctions.delete(listener);
    };
    ReactiveAction.prototype.dispatch = function (value) {
        var dispatchValue = value === null || value === void 0 ? void 0 : value.payload;
        this.ListenerFunctions.forEach(function (listener) { return listener(dispatchValue); });
    };
    return ReactiveAction;
}());
exports.ReactiveAction = ReactiveAction;
var makeAction = function () {
    return new ReactiveAction();
};
exports.makeAction = makeAction;
//# sourceMappingURL=ReactiveAction.js.map