import { PubSubActionInterceptor } from './types';
/**
 * Generic parameter Config is required!
 * ```tsx
 * const fixedPubSub = createFixedPubSub<PubSubConfig>('pubSubName')
 * ```
 */
export declare const createFixedPubSub: <Config extends {
    channel: string;
    dataType: unknown;
} = never>(name: string & (Config extends never ? "You must provide a type parameter" : string), interceptors?: PubSubActionInterceptor[]) => {
    publish: <K extends Config["channel"]>(channel: K, value: Extract<Config, {
        channel: K;
    }>["dataType"]) => void;
    subscribe: <K_1 extends Config["channel"]>(channel: K_1, callback: (value: Readonly<Extract<Config, {
        channel: K_1;
    }>["dataType"]>) => void) => void;
};
