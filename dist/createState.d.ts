import { StateInterceptor, StateValue } from './types';
export declare type State<T> = Readonly<{
    set: (updateState: ((currentState: T) => T) | T) => void;
    subscribe: {
        (withPrev?: false): Readonly<T>;
        (withPrev: true): Readonly<StateValue<T>>;
    };
    get: {
        (withPrev?: false): Readonly<T>;
        (withPrev: true): Readonly<StateValue<T>>;
    };
    reset: () => void;
}>;
export declare const createState: <T>(name: string, initialState: T, interceptors?: Readonly<{
    onInit?: (name: string, value: T, setState: (updateState: T | ((currentState: T) => T)) => void) => void;
    onSet?: (name: string, newValue: T, prevValue: T) => void;
    onReset?: (name: string, value: T, setState: (updateState: T | ((currentState: T) => T)) => void) => void;
}>[]) => Readonly<{
    set: (updateState: T | ((currentState: T) => T)) => void;
    subscribe: {
        (withPrev?: false): Readonly<T>;
        (withPrev: true): Readonly<StateValue<T>>;
    };
    get: {
        (withPrev?: false): Readonly<T>;
        (withPrev: true): Readonly<StateValue<T>>;
    };
    reset: () => void;
}>;
