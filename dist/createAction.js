"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createAction = void 0;
var globals_1 = require("./globals");
var lib_1 = require("./lib");
var utils_1 = require("./utils");
var createAction = function (name, interceptors) {
    if (interceptors === void 0) { interceptors = []; }
    if (globals_1.currentActionsNames.includes(name) && typeof window !== 'undefined') {
        console.warn("Reactive Action with name ".concat(name, " already defined"));
    }
    globals_1.currentActionsNames.push(name);
    var action = (0, lib_1.makeAction)();
    return {
        dispatch: function (value) {
            var dispatchValue = (0, utils_1.deepFreeze)({ payload: value });
            action.dispatch(dispatchValue);
            interceptors.forEach(function (interceptor) { var _a; return (_a = interceptor.onDispatch) === null || _a === void 0 ? void 0 : _a.call(interceptor, name, dispatchValue.payload); });
        },
        subscribe: function (callback) { return (0, lib_1.useReactiveAction)(action, callback); }
    };
};
exports.createAction = createAction;
//# sourceMappingURL=createAction.js.map