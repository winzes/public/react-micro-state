import { PubSubActionInterceptor } from './types';
declare type Action = Readonly<{
    publish: <T>(channel: string | number, value: T) => void;
    subscribe: <T>(channel: string | number, callback: (value: Readonly<T>) => void) => void;
}>;
export declare const createCustomPubSub: (name: string, interceptors?: PubSubActionInterceptor[]) => Action;
export {};
