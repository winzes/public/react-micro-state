export declare type StateInterceptor<T = any> = Readonly<{
    onInit?: (name: string, value: T, setState: (updateState: ((currentState: T) => T) | T) => void) => void;
    onSet?: (name: string, newValue: T, prevValue: T) => void;
    onReset?: (name: string, value: T, setState: (updateState: ((currentState: T) => T) | T) => void) => void;
}>;
export declare type ActionInterceptor<T = any> = Readonly<{
    onDispatch?: (name: string, value: T) => void;
}>;
export declare type PubSubActionInterceptor<T = any> = Readonly<{
    onDispatch?: (name: string, channel: string | number, value: T) => void;
}>;
export declare type StateValue<T> = {
    prev: T | null;
    current: T;
};
