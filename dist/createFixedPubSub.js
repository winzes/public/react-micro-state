"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFixedPubSub = void 0;
var globals_1 = require("./globals");
var lib_1 = require("./lib");
var utils_1 = require("./utils");
/**
 * Generic parameter Config is required!
 * ```tsx
 * const fixedPubSub = createFixedPubSub<PubSubConfig>('pubSubName')
 * ```
 */
var createFixedPubSub = function (name, interceptors) {
    if (interceptors === void 0) { interceptors = []; }
    if (globals_1.currentActionsNames.includes(name) && typeof window !== 'undefined') {
        console.warn("PubSub Action with name ".concat(name, " already defined"));
    }
    globals_1.currentActionsNames.push(name);
    var action = (0, lib_1.makeChannelAction)();
    return {
        publish: function (channel, value) {
            var dispatchValue = (0, utils_1.deepFreeze)({ payload: value });
            action.dispatchToChannel(channel, dispatchValue);
            interceptors.forEach(function (interceptor) { var _a; return (_a = interceptor.onDispatch) === null || _a === void 0 ? void 0 : _a.call(interceptor, name, channel, dispatchValue.payload); });
        },
        subscribe: function (channel, callback) { return (0, lib_1.useReactiveChannelAction)(action, channel, callback); }
    };
};
exports.createFixedPubSub = createFixedPubSub;
//# sourceMappingURL=createFixedPubSub.js.map