import { StateInterceptor } from "./types";
declare type StateMap<T> = Readonly<{
    set: (key: string | number, updateState: ((currentState: T) => T) | T) => void;
    subscribe: (key: string | number) => Readonly<T>;
    subscribeMap: () => Readonly<Omit<Map<string | number, Readonly<T>>, 'set' | 'delete' | 'clear'>>;
    get: (key: string | number) => Readonly<T>;
    getMap: () => Readonly<Omit<Map<string | number, Readonly<T>>, 'set' | 'delete' | 'clear'>>;
    reset: (key: string | number) => void;
    resetMap: () => void;
}>;
export declare const createStateMap: <T>(name: string, defaultValue: T, interceptors?: Readonly<{
    onInit?: (name: string, value: Map<string | number, T>, setState: (updateState: Map<string | number, T> | ((currentState: Map<string | number, T>) => Map<string | number, T>)) => void) => void;
    onSet?: (name: string, newValue: Map<string | number, T>, prevValue: Map<string | number, T>) => void;
    onReset?: (name: string, value: Map<string | number, T>, setState: (updateState: Map<string | number, T> | ((currentState: Map<string | number, T>) => Map<string | number, T>)) => void) => void;
}>[]) => Readonly<{
    set: (key: string | number, updateState: T | ((currentState: T) => T)) => void;
    subscribe: (key: string | number) => Readonly<T>;
    subscribeMap: () => Readonly<Omit<Map<string | number, Readonly<T>>, "set" | "clear" | "delete">>;
    get: (key: string | number) => Readonly<T>;
    getMap: () => Readonly<Omit<Map<string | number, Readonly<T>>, "set" | "clear" | "delete">>;
    reset: (key: string | number) => void;
    resetMap: () => void;
}>;
export {};
