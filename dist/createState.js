"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createState = void 0;
var globals_1 = require("./globals");
var lib_1 = require("./lib");
var utils_1 = require("./utils");
var createState = function (name, initialState, interceptors) {
    if (interceptors === void 0) { interceptors = []; }
    if (globals_1.currentStatesNames.includes(name) && typeof window !== 'undefined') {
        console.warn("Reactive State with name ".concat(name, " already defined"));
    }
    globals_1.currentStatesNames.push(name);
    var state = (0, lib_1.makeVar)((0, utils_1.deepFreeze)({ payload: initialState }));
    var set = function (updateState, initial) {
        var prevValue = state.getValue();
        if (typeof updateState === 'function') {
            var newValue = (0, utils_1.deepFreeze)({ payload: updateState(prevValue) });
            if (newValue.payload === prevValue) {
                return;
            }
            initial ? state.reset(newValue) : state.setValue(newValue);
        }
        else {
            var newValue = (0, utils_1.deepFreeze)({ payload: updateState });
            if (newValue.payload === prevValue) {
                return;
            }
            initial ? state.reset(newValue) : state.setValue(newValue);
        }
        interceptors.forEach(function (interceptor) { var _a; return (_a = interceptor.onSet) === null || _a === void 0 ? void 0 : _a.call(interceptor, name, state.getValue(), prevValue); });
    };
    var reset = function () {
        state.reset((0, utils_1.deepFreeze)({ payload: initialState }));
        interceptors.forEach(function (interceptor) { var _a; return (_a = interceptor.onReset) === null || _a === void 0 ? void 0 : _a.call(interceptor, name, state.getValue(), function (updateState) { return set(updateState, true); }); });
    };
    function get(withPrev) {
        if (withPrev === void 0) { withPrev = false; }
        return !withPrev ? state.getValue() : { prev: state.getPrevValue(), current: state.getValue() };
    }
    function subscribe(withPrev) {
        if (withPrev === void 0) { withPrev = false; }
        return !withPrev ? (0, lib_1.useReactiveVar)(state) : (0, lib_1.useReactiveVarWithPrev)(state);
    }
    interceptors.forEach(function (interceptor) { var _a; return (_a = interceptor.onInit) === null || _a === void 0 ? void 0 : _a.call(interceptor, name, state.getValue(), function (updateState) { return set(updateState, true); }); });
    return {
        get: get,
        set: function (updateState) { return set(updateState); },
        subscribe: subscribe,
        reset: reset
    };
};
exports.createState = createState;
//# sourceMappingURL=createState.js.map