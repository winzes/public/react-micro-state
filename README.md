## Reactive tools for React.js

- **createState** - create state reactive variable that can be get and set from everywhere and subscribed to changes in react components <hr />
- **createStateMap** - create state reactive variable that can be get and set from everywhere and subscribed to changes in react components <hr />
- **createAction** - create action that can be dispatched from everywhere and subscribed in react components <hr />
- **createCustomPubSub** - create a custom PubSub that can publish from everywhere and subscribe by channel in react components <hr />
- **createFixedPubSub** - create a preconfigured PubSub that can publish from everywhere and subscribe by channel in react components

## Examples:

<details><summary>createState</summary>
<hr />

Create a new reactive state of generic type:

```
const exampleState = createState<number>(0);
```
Update the state:
```
exampleState.set(10);
```

Get the state from everywhere: 
```
const example = exampleState.get();
```
Subscribe for state updates in react components: 
```
const example = exampleState.subscribe();
```

Get the state from everywhere ( with prev value ): 
```
const { prev, current } = exampleState.get(true);
```
Subscribe for state updates in react components ( with prev value ): 
```
const { prev, current } = exampleState.subscribe(true);
```

Reset the state from everywhere and return it to initial value: 
```
exampleState.reset();
```
</details>

<details><summary>createStateMap</summary>
<hr />

Create a new reactive state map of generic type:

```
const exampleStateMap = createStateMap<number>(0);
```
Update the state by some key (string | number):
```
exampleStateMap.set('key', 10)
```

Get the state by key from everywhere: 
```
const example = exampleStateMap.get('key')
```
Subscribe for state updates by key in react components: 
```
const example = exampleStateMap.subscribe('key');
```

Get the state of whole map from everywhere: 
```
const example = exampleStateMap.getMap()
```
Subscribe for state updates of whole map in react components: 
```
const example = exampleStateMap.subscribeMap();
```

Reset state by key and return it to initial value: 
```
const example = exampleStateMap.reset('key');
```
Reset state for whole map and return it to initial value: 
```
const example = exampleStateMap.resetMap();
```
</details>

<details><summary>createAction</summary>
<hr />

Create a new reactive action of generic type (can be and void):

```
const exampleAction = createAction<string>();
```

Dispatch action from everywhere: 
```
exampleAction.dispatch("Hi")
```

Subscribe for action in react components
```
exampleAction.subscribe(useCallback((value) => {
    console.log(`Action with value: ${value}`);
}, []))
```
</details>

<details><summary>createCustomPubSub</summary>
<hr />

Create a new PubSub:

```
const customPubSub = createCustomPubSub('customPubSub');
```

Publish to channel from everywhere: 
```
customPubSub.publish<string>('someChannelName', 'World!')
```

Subscribe by channel in react components
```
customPubSub.subscribe<string>("someChannelName", useCallback((value) => {
    console.log(`Hello ${value}`);
}, []))
```

_Also supports void pub/sub for action trigger_
</details>

<details><summary>createFixedPubSub</summary>
<hr />

Create a new PubSub with predefined channel config:

```
type PubSubConfig = {
    channel: "channel1",
    dataType: string,
} | {
    channel: "channel2",
    dataType: number,
}

const fixedPubSub = createFixedPubSub<PubSubConfig>('fixedPubSub')
```

Publish to channel from everywhere: 
```
fixedPubSub.publish('channel1', 'World!')
```

Subscribe by channel in react components
```
fixedPubSub.subscribe("channel1", useCallback((value) => {
    console.log(`Hello ${value}`);
}, []))
```
_Also supports void pub/sub for action trigger_
</details>
